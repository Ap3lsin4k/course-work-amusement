from CourseWorks_Modules import calculate_income, calculate_expenses

show_val = int(input("План реконструкції підприємства. Продемонструвати? Да-1, ні-0 ")))

functions = (
    lambda name, money1, money2: print('function #1'),
    lambda name, money1, money2: print('function #2'),
    lambda name, money1, money2: None,
    lambda name, money1, money2: None,
    calculate_income,
    calculate_expenses,
    lambda name, money1, money2: 0,
    lambda name, money1, money2: 0,
    lambda name, money1, money2: 0,
    lambda name, money1, money2: 0,
)

if show_val == 1:
    reconstruction_plan = [['Показники', 'І варіант', 'ІІ варіант'],
                          ['Основні засоби', 485.0, 685.0],
                          ['Оплата праці', 65.0, 82.0],
                          ['Витрати матеріалу', 52.0, 45.0],
                          ['Випуск продукції', 7250, 8420],
                          ['Ціна деталі', 80, 80],
                          ['Дохід', 0, 0],
                          ['Витрати', 0, 0],
                          ['-оплата праці', 0, 0],
                          ['-соціальний внесок', 0, 0],
                          ['-матеріальні витрати', 0, 0],
                          ['-амортизація', 0, 0],
                          ['Результат до оподаткування', 0, 0],
                          ['Податок на прибуток', 0, 0],
                          ['Чистий фінрезультат', 0, 0],
                          ['Рентабельність', 0, 0]]

    for func, args in zip(functions, reconstruction_plan):
        print(f'Calling {func.__name__} with {tuple(args)} as positional arguments')
        func(*args)
